# Machine Mayhem 2022

---

# Links

- overview
   - https://gitlab.cba.mit.edu/jakeread/machineweek-2022
- modular circuits
   - https://github.com/modular-things/modular-things-circuits
- modular software
   - https://github.com/modular-things/modular-things
- modular axis
   - https://gitlab.cba.mit.edu/quentinbolsee/beehive-axes
- printed axis
   - https://gitlab.fabcloud.org/quentin.bolsee/urumbu_printed_axis
- 2D example
   - https://clank.tools/build/corexy/
- tool changer
   - https://clank.tools/build/hotplate/
- minimal controller
   - https://gitlab.cba.mit.edu/quentinbolsee/urumbu_io

# The Task 

Each section will design and build a "machine" - this means anything that includes **mechanism, actuation, automation and application**, so:

### **Mechanism:** mechanical degrees-of-freedom (DOF)

### **Actuation:** computer-controllable motion of the above DOFs

### **Automation:** software-coordinated computer-control of the above, and i.e. sequences therein 

### **Application:** pipelines from desired (designed) outcomes (shapes generated, patterns drawn, parts picked-and-placed etc), to actual outcomes (real stuff!)

## Layering / Interfacing 

![messy](images/computational-metrology-machine-mess.png)

![interface](images/interfaces-j-q.png)

![layerish](images/machine-systems-setup.png)

---

# Examples / Ideas 

The machines you design and build **don't need to be complicated** - try to de-risk ideas early, and do stuff that "works right away" rather than complex projects that require hundreds of details to come together all at once in order to work. This can be fun; have fun. 

### [Label Maker](https://www.youtube.com/watch?v=Y_rrbo6_42U&t=73s)
### [Wire Cutter](https://youtu.be/7E6iHRjvH_k?t=20)
### [Music Machines](https://youtu.be/dhKYxDzyXqI?t=5)
### [String / Floating Machines](https://youtu.be/dZLtPFJEQi0?t=104)
### [Architecture-Scale / Space Transforming](https://fab.cba.mit.edu/classes/865.21/people/gilsunshine/systems.html)
### [Claw Machine](https://youtu.be/zR3BLM_TAmg?t=117)
### [Robot Chainsaw Machine](https://youtu.be/ix68oRfI5Gw?t=1160) 
### [Robot Basketball Hoop](https://youtu.be/myO8fxhDRW0?t=598) 

---

# Strategies 

- use *interfaces / modularity* to split work up,
- keep your parts, and your tools, organized 
- have a group chat ! invite your TAs !

---

# Hardware

## Composable Axis 

- [Beehive machine building kit](https://gitlab.cba.mit.edu/quentinbolsee/beehive-axes)
- [Clank-Style Rollers](https://clank.tools/build/composable/) 
- [Urumbu-style axis](https://fabacademy.org/2020/labs/ulb/students/quentin-bolsee/projects/urumbu_axis/)

## Composable Rotary

- [Rotary Axis](https://gitlab.cba.mit.edu/jakeread/aaxis) (placeholder)

## Extrusions, Brackets, etc 

- structure, etc

## Machine Week Kit Contents 

Here's the list... with the note that we have extras of most line items, so groups need to get a handle on what they will need and let us know where to restock *early early* ! 

| Part | QTY | Ordered ? | Arrived ? | Kitted ? |
| --- | --- | --- | --- | --- |
| **Hardware** | --- | --- | --- | --- |
| FHCS M5x10 | x | * | * | * |
| FHCS M5x15 | x | * | * | * |
| FHCS M5x20 | x | * | * | * |
| FHCS M5x30 | x | * | * | * |
| SHCS M5x10 | x | * | * | * |
| SHCS M5x14 | x | * | * | * |
| SHCS M5x20 | x | * | * | * |
| SHCS M5x30 | x | * | * | * |
| Nylock M5  | x | * | * | * |
| Washer M5 | x | |
| Extrusion Pre-Install T-Nuts ? | 
| Extrusion Post-Install T-Nuts ? |
| **USB** | --- | --- | --- | --- |
| 7-Port Powered USB Hub | 1 | * | * | * |
| USB Extension Cables | 7 | * | * | * |
| **Transmission** | --- | --- | --- | --- |
| Kevlar 8800K43 | 50ft | * | * | |
| GT2 Pulleys ? |
| GT2 Belt ? |
| GT2 Belt Closed 200mm Circumference | 2 | * | * | * |
| NEMA17 Motors | 5 | * | * | * |
| 6806 Bearings | 2 | * | * | * |
| 625 Bearings | 20 ? | * | * | * |
| **Misc** | --- | --- | --- | --- |
| Optical End-Stops ? (15 total at lab, check home) |

---

# Electronics / Software

based on [urumbu](https://gitlab.cba.mit.edu/quentinbolsee/urumbu-io), which are also available to use, we are supplying circuits and examples for [modular-thing](https://github.com/modular-things/modular-things/) of which there are many [circuits](https://github.com/modular-things/modular-things-circuits) 

--- 

# Useful Design Notes

[How to Make Something that Makes Almost Anything](https://fab.cba.mit.edu/classes/865.21/index.html) 

## MechE

[slocum: FUNdaMENTALS](http://pergatory.mit.edu/resources/fundamentals.html)  
[mechanical design principles](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/principles/)  
[transmissions](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/transmissions/)  
[kinematics](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/kinematics/)  
[materials](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/materials/)  
[common mechanical design patterns](https://fab.cba.mit.edu/classes/865.21/topics/mechanical_design/approaches/)

## EE (power electronics):

[common switching architectures](https://fab.cba.mit.edu/classes/865.21/topics/power_electronics/architectures/)  
[commutating motors](https://fab.cba.mit.edu/classes/865.21/topics/power_electronics/commutation/)

## Metrology: 

[accuracy vs. precision](https://fab.cba.mit.edu/classes/865.21/topics/metrology/01_concepts.html)
